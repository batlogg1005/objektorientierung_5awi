package at.fbat.Objektorientierung;


public class Car {
	private String color;
	private int maxvelocity;
	private float baseprice;
	private float baseconsumption;
	private String model;
	private Producer producer;
	private Engine engine;

	public Car(String color, int maxvelocity, float baseprice, float baseconsumption, String model, Producer producer,
			Engine engine) {
		super();
		this.color = color;
		this.maxvelocity = maxvelocity;
		this.baseprice = baseprice;
		this.baseconsumption = baseconsumption;
		this.model = model;
		this.producer = producer;
		this.engine = engine;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public Engine getEngine() {
		return engine;
	}


	public void setEngine(Engine engine) {
		this.engine = engine;
	}


	public int getMaxvelocity() {
		return maxvelocity;
	}


	public void setMaxvelocity(int maxvelocity) {
		this.maxvelocity = maxvelocity;
	}


	public float getBaseprice() {
		return baseprice;
	}


	public void setBaseprice(float baseprice) {
		this.baseprice = baseprice;
	}


	public float getBaseconsumption() {
		return baseconsumption;
	}


	public void setBaseconsumption(float baseconsumption) {
		this.baseconsumption = baseconsumption;
	}


	public Producer getProducer() {
		return producer;
	}


	public void setProducer(Producer producer) {
		this.producer = producer;
	}
	
	public float getReducedPrice(){
		float reducedPrice = this.baseprice * ((100 - this.producer.getDiscount()) / 100);
		
		return reducedPrice;
	}
	
}
