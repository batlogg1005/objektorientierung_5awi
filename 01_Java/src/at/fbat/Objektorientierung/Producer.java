package at.fbat.Objektorientierung;

public class Producer {
	private String name;
	private String origincountry;
	private float discount;
	
	public Producer(String name, String origincountry, int discount) {
		super();
		this.name = name;
		this.origincountry = origincountry;
		this.discount = discount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrigincountry() {
		return origincountry;
	}

	public void setOrigincountry(String origincountry) {
		this.origincountry = origincountry;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}
	
	
	
}
