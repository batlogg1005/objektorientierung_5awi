package at.fbat.Objektorientierung;

public class Main {
	public static void main(String[] args) {
		Engine e1 = new Engine(150, "diesel");
		Engine e2 = new Engine(100, "benzin");

		Producer prod1 = new Producer("Volkswagen", "Germany", 10);
		
		Car c1 = new Car("black", 170, 5000, 8f, "Polo", prod1, e1);
		Car c2 = new Car("white", 200, 10000, 8, "Golf", prod1, e1);
		//Car c2 = new Car("blue", e2);
	
		//System.out.println(c1.getEngine().getHorsePower());
		
		//System.out.println(c1.getColor());
		//System.out.println(c2.getColor());
		
		Person p1 = new Person ("erwin", "mayer","01012001");
		//Person p2 = new Person ("herbert", "m�ller", "09092002");
		//System.out.println(p2.getAge());
		
		p1.addCar(c1);
		p1.addCar(c2);
		
		System.out.println(p1.getWorth());
		
		//c1.getEngine().setHorsePower(157);
		//System.out.println(e1.getHorsePower());
		
		//System.out.println(Float.toString(c1.getReducedPrice()));
		
		
	}
}
