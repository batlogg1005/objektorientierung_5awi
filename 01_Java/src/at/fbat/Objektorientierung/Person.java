package at.fbat.Objektorientierung;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Person {
	
	private String firstname, lastname;
	private List<Car> cars;
	private Date birthDate;

	public Person(String firstname, String lastname, String date) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.cars = new ArrayList<>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		try {
			this.birthDate = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addCar(Car c) {
		this.cars.add(c);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public int getAge(){
		Date now = new Date();
		long timeBetween = now.getTime() - birthDate.getTime();
		double yearsBetween = timeBetween / 3.15576e+10;
		int age = (int) Math.floor(yearsBetween);
		return age;
	}
	
	public float getWorth() {
		float newWorth = 0;
		for (Car car : cars){
			newWorth = newWorth + car.getReducedPrice();
		}
		
		return newWorth;
	}
	
}
